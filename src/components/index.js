import Vue from 'vue'
import '@/components/vdirective'
import SearchForm from '@/components/bkForm/searchForm.vue'
import BKTable from '@/components/tableForm/bkTable.vue'
import BkBtn from '@/components/bkBtn/index.vue'
import RouterTitle from '@/components/routerTitle'


Vue.component('SearchForm', SearchForm)
Vue.component('BKTable', BKTable)
Vue.component('BkBtn', BkBtn)
Vue.component('RouterTitle', RouterTitle)
