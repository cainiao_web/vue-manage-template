const getters = {
    userInfo: state => state.user.userInfo,
    routeList: state => state.user.routeList,
    barList: state => state.tabBar.barList,
}
export default getters