import {
  signIn,
  signOut,
  getUserInfo
} from '@/api/login'
import router from '@/router';
import routes from '@/router/routes';
import Layout from '@/layout';
import Dashboard from '@/layout/dashboard'

const user = {
  state: {
    userInfo: {},
    routeList: []
  },
  mutations: {
    SET_USER_INFO(state, data) {
      state.userInfo = data
    },
    SET_ROUTE_LIST(state, data) {
      state.routeList = data
    }
  },
  actions: {
    Login({
      commit
    }, content) {
      return new Promise((resolve, reject) => {
        signIn(content).then(res => {
          if (res) {
            sessionStorage.setItem('token', res.token)
            resolve(res)
          }
        })
      })
    },
    GetUserInfo({
      commit,
      dispatch
    }, content) {
      // 动态路由
      // return new Promise((resolve, reject) => {
      //   getUserInfo().then(res => {
      //     if (res) {
      //       commit('SET_USER_INFO', res)
      //       let newRoutes = res.menuList.map(o => {
      //         return {
      //           path: o.menuName ? `/${o.menuName}` : '',
      //           name: o.description,
      //           hidden: o.hidden,
      //           component: Layout,
      //           meta: {
      //             title: o.description,
      //             icon: o.icon
      //           },
      //           children: o.childrenMenu && o.childrenMenu.length && createTree(o.childrenMenu)
      //         }
      //       })
      //       function createTree(menus){
      //         return menus.map(o => {
      //           return {
      //             path: `${o.menuName}`,
      //             name: `${o.description}`,
      //             hidden: o.hidden,
      //             component: (resolve) => require([`@/pages/${o.pageUrl}`], resolve),
      //             meta: {
      //               title: o.description,
      //               icon: o.icon
      //             },
      //             children: o.childrenMenu && o.childrenMenu.length && createTree(o.childrenMenu)
      //           }
      //         })
      //       }
      //       newRoutes.push({
      //         path: '*',
      //         redirect: '/404',
      //         hidden: true
      //       })
      //       console.log(routes, newRoutes)
      //       commit('SET_ROUTE_LIST', routes.concat(newRoutes))
      //       resolve(newRoutes)
      //     } else {
      //       sessionStorage.clear()
      //     }
      //   })
      // })
      // 死路由
      commit('SET_ROUTE_LIST', routes)
      
    },
    LoginOut({
      commit
    }, content) {
      return new Promise((resolve, reject) => {
        signOut(content).then(res => {
          resolve(res)
        })
      })
    }
  }
}

export default user