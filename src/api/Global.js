let BASE_URL = '';

//  BASE_URL = 'http://192.168.101.51:6001/';  // bkgt@123
BASE_URL = 'http://192.168.90.113:7001/'; //武汉中心医院服务器测试环境地址
if (process.env.NODE_ENV == "production") { //用于标准版本前端发布
    BASE_URL = ''
}
let url_name_1 = 'spd-web';
let url_name_2 = 'spd-oauth';
let url_name_3 = 'openapi';
let urlName3 = 'openapi';

// url_name_1 = 'yw-hospital';
// url_name_2 = 'yw-oauth';
// url_name_3 = 'yw-execute';
// urlName3 = 'yw-execute';

const invoiceHospital = ['WUHE']


let IMG_URL = 'gofast'

export {
    BASE_URL,
    url_name_1,
    url_name_2,
    url_name_3,
    urlName3,
    IMG_URL,
    invoiceHospital
}