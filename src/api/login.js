import request from '@/utils/request'
import { url_name_1, url_name_2, url_name_3 } from "@/axios/Global";

export function signIn(data) {
    return request({
        url: `http://192.168.90.124:8055/zyy-oauth/oauth/token?grant_type=password&username=zhoujun&password=MTIzNDU2&client_secret=secret&client_id=zyy-monitor`,
        method: 'post',
    })
}

export function signOut(data) {
    return request({
        url: `${url_name_2}/logout`,
        method: 'GET',
    })
}

export function getUserInfo(data) {
    return request({
        url: `http://192.168.90.124:8055/zyy-oauth/sys-user/initial/1`,
        method: 'GET',
    })
}
