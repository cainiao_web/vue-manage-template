import axios from 'axios'
import store from '@/store/index';
import router from '@/router/index'
import {
    Error,
    errorMsg,
    error
} from '@/axios/common'
import {
    BASE_URL,
    url_name_2
} from "@/axios/Global"
import {
    clearToken
} from '@/utils/auth'
import {
    Message
} from 'element-ui';

function GetRequest(url, key) {
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.split("?")[1];
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    if (theRequest[key] !== undefined) {
        return true;
    } else {
        return false;
    }
}


let pending = []; //声明一个数组用于存储每个请求的取消函数和axios标识
let cancelToken = axios.CancelToken;
let removePending = (config) => {
    for (let i in pending) {
        if (pending[i].url === config.baseURL + config.url) { //在当前请求在数组中存在时执行取消函数
            pending[i].f(); //执行取消操作
            //pending.splice(i, 1); 根据具体情况决定是否在这里就把pending去掉
        }
    }
}

axios.interceptors.request.use(config => {
    removePending(config); //在一个axios发送前执行一下判定操作，在removePending中执行取消操作
    if (!config.url.split('?')[0].includes('sys-user/password/new')) {
        const grant_type_flag = GetRequest(config.url, 'grant_type');
        if (!grant_type_flag) {
            const token = sessionStorage.getItem('TOKEN');
            config.headers.Authorization = `Bearer ${token}`;
        }
        config.headers.depID = sessionStorage.getItem('depID');
        config.cancelToken = new cancelToken(function executor(c) { //本次axios请求的配置添加cancelToken
            pending.push({
                // url: config.url,
                url: config.baseURL + config.url,
                f: c
            });
            //将本次的url添加到pending中，因此对于某个url第一次发起的请求不会被取消，因为还没有配置取消函数
        })
    }
    return Promise.resolve(config)
}, err => {
    return Promise.reject(err);
});



axios.interceptors.response.use(res => {
    // removePending(data.config); //在一个axios响应后再执行一下取消操作，把已经完成的请求从pending中移除
    // if (typeof data.data.status !== 'undefined') {
    //     if (data.data && !data.data.status) {
    //         if (data.data.code === 40102) {
    //             clearToken()
    //             store.dispatch('SetUserInfo', null)
    //             router.push({ path: '/login' })
    //             Error('登录失败，请重新登录')
    //         } else {
    //             error(data.data)
    //         }
    //     }
    // }
    // return data.data
    if (res.data.code == 40102) {
        clearToken()
        store.dispatch('SetUserInfo', null)
        router.push({
            path: '/login'
        })
        Error('登录失败，请重新登录')
    }
    if (res.data.status) {
        if (res.data.code == 999 || res.data.code == -1) {
            Message({
                type: 'info',
                message: res.data.msg
            })
        } else {
            return res.data
        }
    } else {
        Message({
            type: 'error',
            message: res.data.msg
        })
        return false;
    }

}, error => {
    // if (error.response) {
    //     switch (error.response.status) {
    //         case 401:
    //             const token = sessionStorage.getItem('TOKEN');
    //             store.dispatch('Logout', token);
    //             store.dispatch('SetUserInfo', null)
    //             router.push({ path: '/login' });
    //             break;
    //         case 404:
    //             Error(error.response.data.error)
    //             break;
    //         default:
    //             return Promise.reject(error.response.data)
    //     }
    // }
    // return Promise.reject(error.response.data)   // 返回接口返回的错误信息
    if (error.response) {
        Message({
            type: 'error',
            message: error.response.data.msg
        })
    }

    return false;
})
axios.defaults.baseURL = BASE_URL;
export default axios;