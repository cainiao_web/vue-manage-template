import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { BASE_URL } from "@/axios/Global";

// create an axios instance
const service = axios.create({
  // process.env.VUE_APP_BASE_API
  baseURL: BASE_URL, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    config.headers['content-type'] = 'application/json'
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['X-Token'] = getToken()
    }
    if (config.url.split('?')[0].includes('sys-user/password/new') || config.url.split('?')[0].includes('oauth/token') ) {
      return config
    }else{
      const token = sessionStorage.getItem('token');
      config.headers.Authorization = `Bearer ${token}`;
      config.headers.depID = sessionStorage.getItem('depID');
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code === 200||res.code === 201) {
      return res.data
    } else if(res.code === 30112) {
      return res
    } else {
      Message({
        message: res.data || res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 40102 || res.code === 50012 || res.code === 50014) {
        sessionStorage.clear()
        location.reload()
      }
      // return Promise.reject(new Error(res.data || 'Error'))
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)
export default service
