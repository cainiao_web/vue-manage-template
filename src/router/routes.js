
import Layout from '@/layout'
import Dashboard from '@/layout/dashboard'


export default [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/pages/login/index.vue'),
  },
  {
    path: '/404',
    component: () => import('@/pages/404'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: '/home',
    meta: {
      title: 'Home',
      icon: 'table'
    },
    children: [{
      path: 'home',
      name: 'Home',
      component: () => import('@/pages/home.vue'),
      meta: {
        title: 'Home',
        icon: 'table'
      }
    }]
  },
  {
    path: '/example',
    component: Layout,
    name: 'Example',
    meta: {
      title: 'Example',
      icon: 'example'
    },
    children: [{
        // path: 'form',
        path: 'form:id', // 用于多tag
        name: 'Form',
        component: () => import('@/pages/form.vue'),
        meta: {
          title: 'Form',
        }
      },
      {
        path: 'about',
        // path: 'about/:id', // 用于多tag
        name: 'About',
        component: () => import('@/pages/about.vue'),
        meta: {
          title: 'About',
        }
      },
      {
        path: 'more',
        name: 'More',
        component: Dashboard,
        children: [{
          path: 'more',
          name: 'more',
          component: () => import('@/pages/more.vue'),
          meta: {
            title: 'More',
          }
        }]
      }
    ]
  },
]
